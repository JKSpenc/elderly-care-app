import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/services/location_service.dart';
import 'package:checkup/services/messaging_service.dart';
import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart';
import 'package:geolocator/geolocator.dart';

class OKView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(
        appBar: new AppBar(
          automaticallyImplyLeading: false,
          title: new Text("Are you OK?"),
        ),
        body: new OK(),
      );
}

class OK extends StatefulWidget {
  @override
  _OKState createState() => _OKState();
}

class _OKState extends State<OK> {
  Widget getUserContacts() {
    return FutureBuilder<Iterable<Contact>>(
      future: ContactsUtilService.getInstance().getContacts(null, true),
      builder:
          (BuildContext context, AsyncSnapshot<Iterable<Contact>> snapshot) {
        return snapshot.data == null
            ? new Center(
                child: CircularProgressIndicator(),
              )
            : snapshot.data.length == 0
                ? new Text(
                    "Looks like you dont have any emergency contacts set up.")
                : new ListView.builder(
                    itemCount: snapshot.data?.length ?? 0,
                    itemBuilder: (context, index) {
                      Contact c = snapshot.data?.elementAt(index);
                      return ListTile(
                        leading: CircleAvatar(
                            child: Text(ContactsUtilService.getInstance()
                                .getInitials(c))),
                        title: Text(c.name ?? ''),
                        subtitle: Text("Text has been sent!"),
                      );
                    },
                  );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new Expanded(
              child: new RaisedButton(
                child: new Text("OK"),
                color: Colors.green,
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                },
              ),
            ),
            new Padding(
                padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0)),
            new Expanded(
              child: new RaisedButton(
                child: new Text("NOT OK"),
                color: Colors.red,
                onPressed: () async {
                  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
                  AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
                  String separator = ";";
                  if(androidInfo.manufacturer.toLowerCase() =="samsung"){
                    separator = ",";
                  }
                  List<Contact> contacts = List<Contact>.from(await ContactsUtilService.getInstance().getContacts(null, true));

                  String contactList = contacts.fold("", (string, contact) {
                    List<String> mobileNumbers = contact.numbers.where((String number) {
                      return number.contains(new RegExp(r"(\+44|0) *7"));
                    }).toList();
                    String ret = string;
                    if (mobileNumbers.isNotEmpty) {
                      String number = mobileNumbers.first;
                      if (!(contacts.indexOf(contact) == 0)) {
                        number = separator + number;
                      }
                      ret = ret + number;
                    }
                    return ret;
                  });

                  DateTime currentTime = DateTime.now();

                  String msg = "I've checked in as NOT OK at ${currentTime.toString().substring(0, currentTime.toString().lastIndexOf("."))}";

                  Position pos = await LocationService.getInstance().getLocation();

                  if (pos != null) {
                    msg = msg + " and location (${pos.latitude},${pos.longitude})";
                  }

                  MessagingService.getInstance().sendSms(contactList, msg);
//                  showDialog(
//                      context: context,
//                      builder: (BuildContext context) {
//                        return new AlertDialog(
//                          title: Text("Sending Texts"),
//                          content: Container(
//                            width: 100,
//                            child: getUserContacts(),
//                          ),
//                        );
//                      });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
