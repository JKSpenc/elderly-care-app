import 'dart:core';

import 'package:checkup/services/messaging_service.dart';
import 'package:checkup/services/scheduler_service.dart';
import 'package:checkup/ui/common/ui_helper.dart';
import 'package:checkup/ui/schedule_stepper.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class ScheduleCallsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new ScheduleCalls();
}

class ScheduleCalls extends StatefulWidget {
  @override
  _ScheduleCallsState createState() => _ScheduleCallsState();
}

class _ScheduleCallsState extends State<ScheduleCalls> {
  SchedulerService _schedulerService;

  @override
  void initState() {
    super.initState();
    _schedulerService = SchedulerService.getInstance();

    _schedulerService.getActiveCalendar().then((Calendar c) {
      if (c == null) {
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          setState(() {
            UIHelper.setCalendar(context, _schedulerService, "scheduled calls");
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //setCalendar();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Call Scheduling"),
      ),
      body: new Scaffold(
        body: new Padding(
          padding: new EdgeInsets.all(5),
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    flex: 5,
                    child: new Text(
                        "Set up scheduled calls with your emergency contacts here",
                      style: TextStyle(fontSize: 20),),
                  ),
                  new Expanded(
                    flex: 2,
                    child: RaisedButton(
                      child: Text("Change Calendar"),
                      onPressed: () {
                        setState(() {
                          UIHelper.setCalendar(context, _schedulerService, "scheduled calls");
                        });
                      },
                    )
                  )
                ],
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 10, bottom: 10),
              ),
              new Expanded(
                child: UIHelper.getScheduled(context, _schedulerService, SchedulerService.TYPE_CALL),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(builder: ((context) => ScheduleStepper(type: SchedulerService.TYPE_CALL,))),
          );
          Scheduled s;
          if (result.runtimeType == Scheduled) {
            s = result;
          }
          bool success = await SchedulerService.getInstance().schedule(s, SchedulerService.TYPE_CALL);
          setState(() {
            Toast.show(
                success
                    ? "Sucessfully created new scheduled call!"
                    : "Failed to create new scheduled call.",
                context);
          });
          if (success) {
            Calendar currentCal = await _schedulerService.getActiveCalendar();
            var calId = Uri.encodeQueryComponent(currentCal.name);
            var sharableLink =
                "https://calendar.google.com/calendar/embed?src=$calId";
            List<String> mobileNumbers = s.contact.numbers;
            List<String> tmp = mobileNumbers.where((String number) {
              return number.contains(new RegExp(r"(\+44|0) *7"));
            }).toList();
            if (tmp.isNotEmpty) {
              MessagingService.getInstance().sendSms(tmp.first,
                  "I've set up a scheduled call with you! Take a look at when in my calendar here: $sharableLink");
            } else {
              await showDialog<bool>(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("Information"),
                      content: Text(
                          "Tried to send a text to ${s.contact.name} but couldn't find a mobile number to send to."
                              "\n\nThe event has still been created."),
                      actions: <Widget>[
                        FlatButton(
                          child: new Text("Ok"),
                          onPressed: () {
                            Navigator.of(context).pop(true);
                          },
                        ),
                      ],
                    );
                  });
            }
          }
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
