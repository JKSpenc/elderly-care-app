import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/services/scheduler_service.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:toast/toast.dart';

class ScheduleStepper extends StatefulWidget {
  final String type;

  ScheduleStepper({this.type}) : super();

  @override
  _ScheduleStepperState createState() => _ScheduleStepperState(type: type);
}

class _ScheduleStepperState extends State<ScheduleStepper> {
  Scheduled scheduled;

  final String type;

  _ScheduleStepperState({this.type}) : super();

  List<Step> get steps => initSteps();

  Map<int, bool> get validation => {
        0: scheduled.days != null && scheduled.days.isNotEmpty,
        1: true,
        2: true,
        3: type == SchedulerService.TYPE_CALL
            ? scheduled.contact != null
            : scheduled.title != null && scheduled.title.isNotEmpty
      };
  Map<int, bool> errors = {0: false, 1: false, 2: false, 3: false};

  int currentStep = 0;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    scheduled = new Scheduled();
    scheduled.start = DateTime.now();
    scheduled.frequency = 2;
    scheduled.unit = "week(s)";
  }

  List<Step> initSteps() {
    return [
      Step(
        title: Text("Select days"),
        isActive: true,
        state: errors[0] == true ? StepState.error : StepState.indexed,
        content: MultiSelectFormField(
          titleText:
              'Select which days to ${type == SchedulerService.TYPE_CALL ? "schedule a call" : "create a reminder"}',
          autovalidate: true,
          validator: (value) {
            if (value == null || value.length == 0) {
              return 'Please select one or more options';
            }
          },
          dataSource: [
            {
              "display": "Monday",
              "value": DayOfWeek.Monday,
            },
            {
              "display": "Tuesday",
              "value": DayOfWeek.Tuesday,
            },
            {
              "display": "Wednesday",
              "value": DayOfWeek.Wednesday,
            },
            {
              "display": "Thursday",
              "value": DayOfWeek.Thursday,
            },
            {
              "display": "Friday",
              "value": DayOfWeek.Friday,
            },
            {
              "display": "Saturday",
              "value": DayOfWeek.Saturday,
            },
            {
              "display": "Sunday",
              "value": DayOfWeek.Sunday,
            },
          ],
          textField: 'display',
          valueField: 'value',
          value: scheduled.days,
          onSaved: (val) {
            setState(() {
              scheduled.days = List.castFrom(val);
            });
          },
          okButtonLabel: 'OK',
          cancelButtonLabel: 'CANCEL',
          hintText: 'Please choose one or more',
        ),
      ),
      Step(
        title: Text("Set Time"),
        state: errors[1] == true ? StepState.error : StepState.indexed,
        isActive: false,
        content: Row(
          children: <Widget>[
            Align(
                alignment: Alignment.centerLeft,
                child: new Text(
                    "${scheduled.start.toString().substring(0, scheduled.start.toString().lastIndexOf("."))}")),
            Padding(
              padding: EdgeInsets.only(left: 5),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: new RaisedButton(
                  child: new Text("Set Time"),
                  onPressed: () async {
                    DateTime newDateTime = await showDatePicker(
                      context: context,
                      firstDate: DateTime.now(),
                      lastDate: DateTime(DateTime.now().year + 10),
                      initialDate: DateTime.now(),
                    );

                    TimeOfDay newTime = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.now(),
                    );

                    setState(() {
                      if (newDateTime != null && newTime != null) {
                        scheduled.start = DateTime(
                            newDateTime.year,
                            newDateTime.month,
                            newDateTime.day,
                            newTime.hour,
                            newTime.minute);
                      }
                    });
                  }),
            ),
          ],
        ),
      ),
      Step(
          title: Text("Set interval"),
          isActive: false,
          state: errors[2] == true ? StepState.error : StepState.indexed,
          content: Row(
            children: <Widget>[
              Text("Every"),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 0, horizontal: 5),
              ),
              DropdownButton<int>(
                value: scheduled.frequency,
                items: <int>[1, 2, 3, 4].map<DropdownMenuItem<int>>((int val) {
                  return DropdownMenuItem<int>(
                    value: val,
                    child: Text(val.toString()),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    scheduled.frequency = newVal;
                  });
                },
              ),
              new Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0)),
              DropdownButton<String>(
                value: scheduled.unit,
                items: <String>["week(s)", "month(s)"]
                    .map<DropdownMenuItem<String>>((String val) {
                  return DropdownMenuItem<String>(
                    value: val,
                    child: Text(val),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    scheduled.unit = newVal;
                  });
                },
              )
            ],
          )),
      Step(
          title: Text(
              "Set ${type == SchedulerService.TYPE_CALL ? "Contact" : "Medication Name"}"),
          isActive: false,
          state: errors[3] == true ? StepState.error : StepState.indexed,
          content: Row(
            children: type == SchedulerService.TYPE_CALL
                ? <Widget>[
                    FutureBuilder<Iterable<Contact>>(
                        future: ContactsUtilService.getInstance()
                            .getContacts(null, true),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return CircularProgressIndicator();
                          } else {
                            return DropdownButton<Contact>(
                              value: scheduled.contact,
                              items: snapshot.data
                                  .map<DropdownMenuItem<Contact>>(
                                      (Contact contact) {
                                return DropdownMenuItem<Contact>(
                                  value: contact,
                                  child: Text(contact.name),
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                setState(() {
                                  scheduled.contact = newVal;
                                });
                              },
                            );
                          }
                        }),
                  ]
                : [
                    new Flexible(
                      child: Form(
                        key: _formKey,
                        child: TextFormField(
                          onChanged: (val) {
                            setState(() {
                              scheduled.title = val;
                            });
                          },
                          validator: (val) {
                            if (val.isEmpty) {
                              return "Please enter a medication.";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ],
          ))
    ];
  }

  next() {
    if (currentStep + 1 != steps.length) {
      goTo(currentStep + 1);
    } else {
      [0, 1, 2, 3].forEach((stepNum) {
        if (validate(stepNum)) {
          setErrored(stepNum, false);
        } else {
          setErrored(stepNum, true);
        }
      });
      errors.values.where((bool) => bool).isEmpty
          ? Navigator.pop(context, scheduled)
          : Toast.show("Please fix the errors before continuing", context);
    }
  }

  cancel() {
    if (currentStep > 0) {
      goTo(currentStep - 1);
    }
  }

  goTo(int step) {
    setState(() => currentStep = step);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            "New ${type == SchedulerService.TYPE_CALL ? "Scheduled Call" : "Medication Reminder"}"),
      ),
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: new Column(
          children: <Widget>[
            Expanded(
              child: Stepper(
                steps: steps,
                currentStep: currentStep,
                onStepContinue: () {
                  if (currentStep == 3 && type == SchedulerService.TYPE_MED_REMINDER) {
                    _formKey.currentState.validate();
                  }
                  if (validate(currentStep)) {
                    setErrored(currentStep, false);
                    next();
                  } else {
                    setErrored(currentStep, true);
                  }
                },
                onStepTapped: (step) => goTo(step),
                onStepCancel: cancel,
              ),
            )
          ],
        ),
      ),
    );
  }

  bool validate(int currentStep) {
    return validation[currentStep];
  }

  void setErrored(int currentStep, bool errState) {
    setState(() {
      errors[currentStep] = errState;
    });
  }
}
