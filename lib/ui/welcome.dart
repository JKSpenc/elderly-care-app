import 'package:flutter/material.dart';

class WelcomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(
        appBar: new AppBar(
          automaticallyImplyLeading: false,
          title: new Text("Welcome!"),
        ),
        body: new Welcome(),
      );
}

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: new Column(
          children: <Widget>[
            new Text(
              "Welcome to Checkup. To begin, you will need to set up some "
                  "emergency contacts. Press the button below to get started.",
              style: TextStyle(fontSize: 28),
            ),
            Padding(padding: EdgeInsets.symmetric(vertical: 20),),
            new RaisedButton(
              padding: EdgeInsets.all(10),
              child: new Text("Continue" ,style: TextStyle(fontSize: 20),),
              onPressed: () {
                Navigator.pushNamed(context, '/contacts');
              },
            ),
          ],
        ),
      ),
    );
  }
}
