import 'package:checkup/services/contacts_service.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/material.dart';

class AddContactsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(
        appBar: new AppBar(
          title: new Text("Add Contacts"),
        ),
        body: new AddContacts(),
      );
}

class AddContacts extends StatefulWidget {
  @override
  _AddContactsState createState() => _AddContactsState();
}

class _AddContactsState extends State<AddContacts> {
  ContactsUtilService _contactsService;

  @override
  void initState() {
    super.initState();
    _contactsService = ContactsUtilService.getInstance();
  }

  Future<List<Contact>> getUserContacts(String search) {
    return _contactsService.getContacts(search, false).asStream().map((it) => List<Contact>.from(it)).first;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: SearchBar<Contact>(
          onSearch: getUserContacts,
          loader: Center(
            child: CircularProgressIndicator(),
          ),
          emptyWidget: Center(
            child: Text("No contacts found!"),
          ),
          onItemFound: (Contact c, int index) {
            return ListTile(
              leading:
                  CircleAvatar(child: Text(_contactsService.getInitials(c))),
              title: Text(c.name ?? ''),
              subtitle: Text(
                  c.numbers.toString().replaceAll("[", "").replaceAll("]", "")),
              trailing: IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    _contactsService.insertContact(c);
                    Navigator.pop(context);
                  });
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
