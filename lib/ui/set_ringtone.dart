import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/services/ringtones_service.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:url_audio_stream/url_audio_stream.dart';

class SetRingtoneView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Contact currentContact = ModalRoute.of(context).settings.arguments;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Set a ringtone"),
      ),
      body: new SetRingtone(
        currentContact: currentContact,
      ),
    );
  }
}

class SetRingtone extends StatefulWidget {
  final Contact currentContact;

  SetRingtone({this.currentContact}) : super();

  @override
  _SetRingtoneState createState() => _SetRingtoneState();
}

class _SetRingtoneState extends State<SetRingtone> {
  RingtonesService _ringtoneService;
  AudioStream stream;
  String currentlyPlaying;

  @override
  void initState() {
    super.initState();
    _ringtoneService = RingtonesService.getInstance();
  }

  Future<List<Ringtone>> getRingtones(String search) {
    return _ringtoneService
        .getRingtones(search)
        .asStream()
        .map((it) => List<Ringtone>.from(it))
        .first;
  }

  bool isStringEmpty(String str) {
    return (str == null || str.trim().isEmpty);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: SearchBar<Ringtone>(
          onSearch: getRingtones,
          loader: Center(
            child: CircularProgressIndicator(),
          ),
          emptyWidget: Center(
            child: Text("No ringtones found!"),
          ),
          onError: (error) {
            return Center(
              child: Text("Error occurred : $error"),
            );
          },
          onItemFound: (Ringtone r, int index) {
            return ListTile(
              leading: IconButton(
                icon: r.id == currentlyPlaying
                    ? Icon(Icons.pause_circle_outline)
                    : Icon(Icons.play_circle_outline),
                onPressed: () {
                  if (currentlyPlaying == null || r.id != currentlyPlaying) {
                    if (stream != null) {
                      stream.stop();
                    }
                    setState(() {
                      stream = new AudioStream(r.path);
                      currentlyPlaying = r.id;
                    });

                    stream.start();
                  } else {
                    stream.stop();
                    setState(() {
                      currentlyPlaying = null;
                    });
                  }
                },
              ),
              title: Text(r.title ?? ''),
              trailing: IconButton(
                  icon: Icon(Icons.add),
                  onPressed: ()  {
                    _ringtoneService.setRingtone(r, widget.currentContact);
                    Navigator.pop(context);
                  }),
            );
          },
        ),
      ),
    );
  }
}
