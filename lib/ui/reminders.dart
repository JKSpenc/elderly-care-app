import 'package:checkup/services/scheduler_service.dart';
import 'package:checkup/ui/common/ui_helper.dart';
import 'package:checkup/ui/schedule_stepper.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class RemindersView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Reminders();
}

class Reminders extends StatefulWidget {
  @override
  _RemindersState createState() => _RemindersState();
}

class _RemindersState extends State<Reminders> {
  SchedulerService _schedulerService;

  @override
  void initState() {
    super.initState();
    _schedulerService = SchedulerService.getInstance();

    _schedulerService.getActiveCalendar().then((Calendar c) {
      if (c == null) {
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          setState(() {
            UIHelper.setCalendar(
                context, _schedulerService, "medication reminders");
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //setCalendar();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Medication Reminders"),
      ),
      body: new Scaffold(
        body: new Padding(
          padding: new EdgeInsets.all(5),
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    flex: 5,
                    child: new Text(
                        "Set up reminders to take your medication here",
                      style: TextStyle(fontSize: 20),),
                  ),
                  new Expanded(
                      flex: 2,
                      child: RaisedButton(
                        child: Text("Change Calendar"),
                        onPressed: () {
                          setState(() {
                            UIHelper.setCalendar(
                                context, _schedulerService, "scheduled calls");
                          });
                        },
                      ))
                ],
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 10, bottom: 10),
              ),
              new Expanded(
                child: UIHelper.getScheduled(context, _schedulerService,
                    SchedulerService.TYPE_MED_REMINDER),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(builder: ((context) => ScheduleStepper(type: SchedulerService.TYPE_MED_REMINDER,))),
          );
          Scheduled s;
          if (result.runtimeType == Scheduled) {
            s = result;
          }
          bool success = await SchedulerService.getInstance()
              .schedule(s, SchedulerService.TYPE_MED_REMINDER);
          setState(() {
            Toast.show(
                success
                    ? "Sucessfully created a new medicine reminder!"
                    : "Failed to create a new medicine reminder.",
                context);
          });
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
