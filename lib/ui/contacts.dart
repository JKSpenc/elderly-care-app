import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/ui/home.dart';
import 'package:flutter/material.dart';

class EmergencyContactsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: () async {
      if (await ContactsUtilService.getInstance().hasEmergencyContacts()) {
        Navigator.of(context).pushReplacementNamed('/home');
        return false;
      } else {
        Navigator.of(context).pushReplacementNamed('/welcome');
        return false;
      }
    },
    child: new Scaffold(
      appBar: new AppBar(
        title: new Text("Emergency Contacts"),
      ),
      body: new EmergencyContacts(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/add_contacts');
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    ),
  );
}

class EmergencyContacts extends StatefulWidget {
  @override
  _AccessContactsState createState() => _AccessContactsState();
}

class _AccessContactsState extends State<EmergencyContacts> {
  ContactsUtilService _contactsService;

  @override
  void initState() {
    super.initState();
    _contactsService = ContactsUtilService.getInstance();
  }

  Widget getUserContacts() {
    return FutureBuilder<Iterable<Contact>>(
      future: _contactsService.getContacts(null, true),
      builder:
          (BuildContext context, AsyncSnapshot<Iterable<Contact>> snapshot) {
        return snapshot.data == null
            ? new Center(
                child: CircularProgressIndicator(),
              )
            : snapshot.data.length == 0
                ? new Text(
                    "Looks like you dont have any emergency contacts set up."
                    " Press the + in the bottom right hand corner to set some up.",
                    style: TextStyle(fontSize: 26),
                  )
                : new ListView.builder(
                    itemCount: snapshot.data?.length ?? 0,
                    itemBuilder: (context, index) {
                      Contact c = snapshot.data?.elementAt(index);
                      return ListTile(
                        leading: CircleAvatar(
                            child: Text(_contactsService.getInitials(c))),
                        title: Text(c.name ?? ''),
                        subtitle: Text(c.numbers
                            .toString()
                            .replaceAll("[", "")
                            .replaceAll("]", "")),
                        trailing: IconButton(
                          icon: Icon(Icons.remove),
                          onPressed: () {
                            setState(() {
                              _contactsService.deleteContact(c.id);
                            });
                          },
                        ),
                      );
                    },
                  );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Expanded(
                  flex: 6,
                  child: new Text(
                      "Press the ? if you want more information on how these are used",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                new Expanded(
                  flex: 1,
                  child: IconButton(
                    icon: Icon(Icons.help),
                    color: Colors.grey,
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: new Text(
                                "Emergency Contacts",
                                style: TextStyle(fontSize: 30),
                              ),
                              content: new Text(
                                "Setting up emergency contacts allows a text to "
                                "be sent to all emergency contacts with"
                                " your current location when you press the "
                                "'NOT OK' menu "
                                "option.",
                                style: TextStyle(fontSize: 20),
                              ),
                              actions: <Widget>[
                                new RaisedButton(
                                  child: new Text("Understood."),
                                  onPressed: () => Navigator.pop(context),
                                ),
                              ],
                            );
                          });
                    },
                  ),
                ),
              ],
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 10, bottom: 10),
            ),
            new Expanded(
              child: getUserContacts(),
            ),
          ],
        ),
      ),
    );
  }
}
