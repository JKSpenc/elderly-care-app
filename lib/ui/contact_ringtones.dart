import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/services/ringtones_service.dart';
import 'package:flutter/material.dart';

class ContactRingtonesView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(
        appBar: AppBar(
          title: new Text("Set Ringtones"),
        ),
        body: new ContactRingtones(),
      );
}

class ContactRingtones extends StatefulWidget {
  @override
  _ContactRingtonesState createState() => _ContactRingtonesState();
}

class _ContactRingtonesState extends State<ContactRingtones> {
  ContactsUtilService _contactsService;
  RingtonesService _ringtoneService;

  @override
  void initState() {
    super.initState();
    _contactsService = ContactsUtilService.getInstance();
    _ringtoneService = RingtonesService.getInstance();
  }

  Widget getRingtoneName(Contact c) {
    return FutureBuilder<String>(
        future: _ringtoneService.getCurrentRingtone(c),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) =>
            snapshot.connectionState == ConnectionState.waiting
                ? CircularProgressIndicator()
                : snapshot.data != '' ? new Text("Ringtone already set!") : new Text("No ringtone set for ${c.name}"));
  }

  Widget getUserContacts() {
    return FutureBuilder<Iterable<Contact>>(
      future: _contactsService.getContacts(null, true),
      builder:
          (BuildContext context, AsyncSnapshot<Iterable<Contact>> snapshot) {
        return snapshot.data == null
            ? new Center(
                child: CircularProgressIndicator(),
              )
            : snapshot.data.length == 0
                ? new Text(
                    "Looks like you dont have any emergency contacts set up."
                    " Press the + in the bottom right hand corner to set some up.",
                    style: TextStyle(fontSize: 25),
                  )
                : new ListView.builder(
                    itemCount: snapshot.data?.length ?? 0,
                    itemBuilder: (context, index) {
                      Contact c = snapshot.data?.elementAt(index);
                      return ListTile(
                        leading: CircleAvatar(
                            child: Text(_contactsService.getInitials(c))),
                        title: Text(c.name ?? ''),
                        subtitle: getRingtoneName(c),
                        trailing: new RaisedButton(
                          child: new Text("Set Ringtone"),
                          onPressed: () => Navigator.pushNamed(
                              context, '/set_ringtone',
                              arguments: c),
                        ),
                        onTap: () => Navigator.pushNamed(
                            context, '/set_ringtone',
                            arguments: c),
                      );
                    },
                  );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Padding(
        padding: new EdgeInsets.all(5),
        child: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Expanded(
                  flex: 6,
                  child: new Text(
                      "Press the ? if you want more information on how these are used",
                    style: TextStyle(fontSize: 20),),
                ),
                new Expanded(
                  flex: 1,
                  child: IconButton(
                    icon: Icon(Icons.help),
                    color: Colors.grey,
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: new Text(
                                "Set Ringtones",
                                style: TextStyle(fontSize: 30),
                              ),
                              content: new Text(
                                "Here you can set ringtones for your emergency "
                                "contacts, so that you know who is ringing based"
                                " off a unique ringtone.",
                                style: TextStyle(fontSize: 20),
                              ),
                              actions: <Widget>[
                                new RaisedButton(
                                  child: new Text("Understood."),
                                  onPressed: () => Navigator.pop(context),
                                ),
                              ],
                            );
                          });
                    },
                  ),
                ),
              ],
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 10, bottom: 10),
            ),
            new Expanded(
              child: getUserContacts(),
            ),
          ],
        ),
      ),
    );
  }
}
