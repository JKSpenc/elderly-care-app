import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/services/messaging_service.dart';
import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => WillPopScope(
    onWillPop: () async {
      if (await ContactsUtilService.getInstance().hasEmergencyContacts()) {
        Navigator.of(context).pushReplacementNamed('/ok');
        return false;
      } else {
        Navigator.of(context).pushReplacementNamed('/welcome');
        return false;
      }
    },
    child: new Scaffold(
      appBar: new AppBar(
        title: new Text("Welcome"),
      ),
      body: new Home(),
    ),
  );
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new Padding(
      padding: EdgeInsets.all(5),
      child: ListView(
        children: <Widget>[
          constructListItem(context, "Setup Emergency Contacts",
              "View and set up emergency contacts", "/contacts"),
          constructListItem(
              context,
              "Set Ringtones",
              "Setup unique ringtones for each of your contacts",
              "/contact_ringtones"),
          constructListItem(context, "Call Scheduling",
              "View and set up scheduled calls", "/schedule_calls"),
          constructListItem(context, "Medication Reminders",
              "View and set up medication reminders", "/reminders"),
          constructListItemWithoutRoute(
              context,
              "Call 111",
              "Connects you to an operator where you can get non emergency health advice",
              () => showCall111Dialog()),
        ],
      ),
    ));
  }

  Card constructListItem(
      BuildContext context, String name, String subtitle, String route) {
    return Card(
      child: ListTile(
        title: new Text(name),
        subtitle: Text(subtitle),
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        onTap: () => Navigator.pushNamed(context, route),
      ),
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
    );
  }

  Card constructListItemWithoutRoute(
      BuildContext context, String name, String subtitle, Function fn) {
    return Card(
      child: ListTile(
        title: new Text(name),
        subtitle: Text(subtitle),
        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        onTap: fn,
      ),
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
    );
  }

  void showCall111Dialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(
              "Call 111",
              style: TextStyle(fontSize: 30),
            ),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  new Text(
                    "Connects you to an operator where you can get non emergency health advice.\n\n"
                    "NHS 111 is available 24 hours a day, 7 days a week.\n\n"
                    "If you have difficulties communicating or hearing, you can:\n"
                    "- call 18001 111 on a textphone\n"
                    "- use the NHS 111 British Sign Language (BSL) interpreter service if you’re deaf and want to use the phone service.\n\n\n"
                    "For more information, please visit: https://www.nhs.uk/using-the-nhs/nhs-services/urgent-and-emergency-care/nhs-111/",
                    style: TextStyle(fontSize: 16),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 0),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 80.0,
                        width: 80.0,
                        child: FloatingActionButton(
                          child: Icon(Icons.call, size: 40),
                          backgroundColor: Colors.green,
                          onPressed: () => MessagingService.getInstance()
                              .openCallWindow("111"),
                        ),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 30),
                      ),
                      Container(
                        height: 80.0,
                        width: 80.0,
                        child: FloatingActionButton(
                          child: Icon(Icons.clear, size: 40),
                          backgroundColor: Colors.red,
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.center,
                  ),
                ],
              ),
            ),
          );
        });
  }
}
