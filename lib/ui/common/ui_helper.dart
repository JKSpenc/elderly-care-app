
import 'package:checkup/services/scheduler_service.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UIHelper {

  static void setCalendar(BuildContext context, SchedulerService service, String usecase) async {
    await showDialog<Calendar>(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return new AlertDialog(
            title:
            new Text("Select which calendar to save $usecase into"),
            content: FutureBuilder<List<Calendar>>(
              future: service.getCalendars(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Calendar>> snapshot) {
                return snapshot.data == null
                    ? new Center(
                  child: CircularProgressIndicator(),
                )
                    : snapshot.data.length == 0
                    ? new RichText(
                  text: TextSpan(children: [
                    TextSpan(
                        text:
                        "No calendars were found. If you want to use "
                            "$usecase, please set up a calendar"
                            " outside this app. More information can"
                            " be found here:\n",
                        style: new TextStyle(color: Colors.black)),
                    TextSpan(
                        text:
                        "https://support.google.com/calendar/answer/2465776?co=GENIE.Platform%3DAndroid&hl=en",
                        style: new TextStyle(color: Colors.blue),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            launch(
                                'https://support.google.com/calendar/answer/2465776?co=GENIE.Platform%3DAndroid&hl=en');
                          })
                  ]),
                )
                    : Container(
                  width: 100,
                  child: ListView.builder(
                    itemCount: snapshot.data?.length ?? 0,
                    itemBuilder: (context, index) {
                      Calendar c = snapshot.data?.elementAt(index);
                      return ListTile(
                        leading: Icon(Icons.calendar_today),
                        title: Text(c.name ?? ''),
                        onTap: () {
                          service.setActiveCalendar(c);
                          Navigator.pop(context);
                        },
                      );
                    },
                  ),
                );
              },
            ),
          );
          //
        });
  }

  static Widget getScheduled(Element el, SchedulerService service, String type) {
    return FutureBuilder<List<Event>>(
      future: service.getScheduled(type),
      builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
        return snapshot.data == null
            ? new Center(
          child: CircularProgressIndicator(),
        )
            : snapshot.data.length == 0
            ? new Text(
          "Looks like you don't have any ${type == SchedulerService.TYPE_CALL ? "calls scheduled" : "medication reminders set up"}."
              " Press the + in the bottom right hand corner to set some up.",
          style: TextStyle(fontSize: 25),
        )
            : new ListView.builder(
          itemCount: snapshot.data?.length ?? 0,
          itemBuilder: (context, index) {
            Event e = snapshot.data?.elementAt(index);
            return ListTile(
              title: Text(e.title ?? ''),
              subtitle: Text(
                  "Every ${e.recurrenceRule?.interval ?? 0} ${e.recurrenceRule?.recurrenceFrequency == RecurrenceFrequency.Weekly ? "week(s)" : "month(s)"}"),
              trailing: IconButton(
                icon: Icon(Icons.remove),
                onPressed: () async {
                  bool confirmed = await showDialog<bool>(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text("Confirm"),
                          content: Text(
                              "Are you sure you want to remove the ${e.title}?"),
                          actions: <Widget>[
                            FlatButton(
                              child: new Text("Yes"),
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                            ),
                            FlatButton(
                              child: new Text("No"),
                              onPressed: () {
                                Navigator.of(context).pop(false);
                              },
                            )
                          ],
                        );
                      });

                  if (confirmed) {
                      service.removedScheduled(e);
                      el.markNeedsBuild();
                  }
                },
              ),
            );
          },
        );
      },
    );
  }

}