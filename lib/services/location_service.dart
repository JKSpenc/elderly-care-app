import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

class LocationService {
  static LocationService _instance;

  static LocationService getInstance() {
    if (_instance == null) {
      _instance = new LocationService();
    }
    return _instance;
  }

  Future<Position> getLocation() async {
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted) {
//      Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
      Geolocator geolocator = Geolocator();
      return geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    } else {
      throw PlatformException(
        code: 'PERMISSION_DENIED',
        message: 'You need to enable access to your contacts.',
        details: null,
      );
    }
  }

  Future<PermissionStatus> _getPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.locationWhenInUse);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.neverAskAgain) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
      await PermissionHandler()
          .requestPermissions([PermissionGroup.locationWhenInUse]);
      return permissionStatus[PermissionGroup.locationWhenInUse] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }
}
