import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class MessagingService {
  static const MethodChannel _channel =
      const MethodChannel('spenc/setringtone');

  static MessagingService _instance;

  static MessagingService getInstance() {
    if (_instance == null) {
      _instance = new MessagingService();
    }
    return _instance;
  }

  void sendSms(String contactNumber, String messageText) async {
    if (contactNumber != null && messageText != null) {
      try {
        var args = <String, dynamic>{
          'messageText': messageText,
          'contactNumber': contactNumber
        };
        await _channel.invokeMethod("sendText", args);
      } on PlatformException catch (e) {
        print("error : $e");
      }
    }
  }

  void openCallWindow(String number) async {
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted) {
      if (number != null) {
        try {
          var args = <String, dynamic>{'contactNumber': number};
          await _channel.invokeMethod("openCallIntent", args);
        } on PlatformException catch (e) {
          print("error : $e");
        }
      }
    } else {
      throw PlatformException(
        code: 'PERMISSION_DENIED',
        message: 'You need to enable access to your phone.',
        details: null,
      );
    }
  }

  Future<PermissionStatus> _getPermission() async {
    PermissionStatus permission =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.phone);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.neverAskAgain) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler().requestPermissions([PermissionGroup.phone]);
      return permissionStatus[PermissionGroup.phone] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }
}
