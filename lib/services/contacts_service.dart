import 'dart:async';

import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sqflite/sqflite.dart';

class Contact {
  final int id;
  final String name;
  final List<String> numbers;

  Contact({this.id, this.name, this.numbers});

  Map<String, dynamic> toMap() {
    return {'id': id, 'name': name, 'numbers': numbers};
  }

  @override
  String toString() {
    return 'Contact{id: $id, name: $name, numbers: $numbers}';
  }

  @override
  bool operator ==(other) {
    return other.runtimeType == Contact && other.id == this.id;
  }
}

class ContactsUtilService {
  static ContactsUtilService _instance;

  Future<Database> _db;

  static ContactsUtilService getInstance() {
    if (_instance == null) {
      _instance = new ContactsUtilService();
      _instance._db = _setupDatabase();
    }
    return _instance;
  }

  static Future<Database> _setupDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'contacts_database.db'),
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        db.execute(
            '''CREATE TABLE contact(id INTEGER PRIMARY KEY, name TEXT);''');
        db.execute('''CREATE TABLE number(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            contactId INTEGER,
            number TEXT);''');
      },
      version: 2,
    );
  }

  Future<Iterable<Contact>> getContacts(
      String query, bool databaseContacts) async {
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted) {
      Iterable contactsOnPhone =
          await ContactsService.getContacts(query: query);
      Iterable<Contact> userContacts =
          List.from(contactsOnPhone).map((contact) {
        Iterable<Item> phoneNums = contact.phones;
        return new Contact(
          id: int.parse(contact.identifier),
          name: contact.displayName,
          numbers: phoneNums.map((Item phone) => phone.value).toList(),
        );
      }).toList();

      Iterable<Contact> dbContacts = await this.databaseContacts();

      if (!databaseContacts) {
        return Future.value(
            userContacts.where((contact) => !dbContacts.contains(contact)));
      } else {
        return Future.value(dbContacts);
      }
    } else {
      throw PlatformException(
        code: 'PERMISSION_DENIED',
        message: 'You need to enable access to your contacts.',
        details: null,
      );
    }
  }

  Future<PermissionStatus> _getPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.neverAskAgain) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.contacts]);
      return permissionStatus[PermissionGroup.contacts] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  Future<void> insertContact(Contact contact) async {
    Database db = await _db;

    await db.insert(
      'contact',
      {'id': contact.id, 'name': contact.name},
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    for (String phoneNum in contact.numbers) {
      await db.insert(
        'number',
        {'contactId': contact.id, 'number': phoneNum},
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }

  Future<List<Contact>> databaseContacts() async {
    // Get a reference to the database.
    final Database db = await _db;

    final List<Map<String, dynamic>> contacts = await db.query('contact');

    List<Map<String, dynamic>> mutableContacts = new List<Map<String, dynamic>>();

    for (Map<String, dynamic> contact in contacts) {
      List<String> numbers = [];

      List<Map<String, dynamic>> storedNumbers = await db.query('number',
              where: 'contactId = ?', whereArgs: [Map.from(contact).remove('id')]);

      Map<String, dynamic> mutableContact = Map.from(contact);
      for (Map<String, dynamic> number in storedNumbers) {
        numbers.add(Map.from(number).remove('number'));
      }
      mutableContact['numbers'] = numbers;
      mutableContacts.add(mutableContact);
    }

    return List.generate(mutableContacts.length, (i) {
      return Contact(
          id: mutableContacts[i]['id'],
          name: mutableContacts[i]['name'],
          numbers: mutableContacts[i]['numbers']);
    });
  }

  Future<void> updateContact(Contact contact) async {
    final db = await _db;

    await db.update(
      'contact',
      {'id': contact.id, 'name': contact.name},
      where: "id = ?",
      whereArgs: [contact.id],
    );
    for (String phoneNum in contact.numbers) {
      await db.update(
        'number',
        {'contactId': contact.id, 'number': phoneNum},
        where: "contactId = ?",
        whereArgs: [contact.id],
      );
    }
  }

  Future<void> deleteContact(int id) async {
    final db = await _db;

    await db.delete(
      'contact',
      where: "id = ?",
      whereArgs: [id],
    );
    await db.delete(
      'number',
      where: "contactId = ?",
      whereArgs: [id],
    );
  }

  String getInitials(Contact c){
    List<String> splitStr = c.name.split(" ");
    return (splitStr.first == splitStr.last) ? splitStr.first[0].toUpperCase() :
    splitStr.first[0].toUpperCase() + splitStr.last[0].toUpperCase();
  }

  Future<bool> hasEmergencyContacts() async {
    List contacts = await databaseContacts();
    return contacts.isNotEmpty;
  }
}
