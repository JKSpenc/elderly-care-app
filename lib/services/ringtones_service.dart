import 'dart:convert';

import 'package:checkup/services/contacts_service.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';

class Ringtone {
  final String id;
  final String title;
  final String path;
  final int size;

  Ringtone({this.id, this.title, this.path, this.size});

  Map<String, dynamic> toMap() {
    return {'id': id, 'title': title, 'path': path, 'size': size};
  }

  @override
  String toString() {
    return 'Ringtone{id: $id, title: $title, path: $path, size: $size}';
  }

  @override
  bool operator ==(other) {
    return other.runtimeType == Ringtone && other.id == this.id;
  }
}

class RingtonesService {
  static const MethodChannel _channel =
      const MethodChannel('spenc/setringtone');

  static RingtonesService _instance;

  static RingtonesService getInstance() {
    if (_instance == null) {
      _instance = new RingtonesService();
    }
    return _instance;
  }

  Future<Iterable<Ringtone>> getRingtones(String search) async {
    String url = Uri.encodeFull('https://www.zedge.net/api-zedge-web/browse/'
        'search?query=$search&section=search-ringtones-$search'
        '&contentType=ringtones');

    var response = await http.get(url);
    Map<String, dynamic> json = jsonDecode(response.body);
    Iterable songArr = json.remove('items');
    List<Ringtone> ringtoneList = songArr
        .map((dynamic d) => new Ringtone(
            id: d.remove('id'),
            title: d.remove('title'),
            path: d.remove('audioUrl'),
            size: d.remove('size')))
        .where((Ringtone r) => (r.path != null && r.path.isNotEmpty))
        .toList();
    return ringtoneList;
  }

  Future<void> setRingtone(Ringtone r, Contact contact) async {
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted) {
      var contactNum = contact.numbers.first;
      try {
        var args = <String, dynamic>{'path': r.path, 'contactNumber': contactNum};
        await _channel.invokeMethod("setRingtone", args);
      } on PlatformException catch (e) {
        print("error : $e");
      }
    } else {
      throw PlatformException(
        code: 'PERMISSION_DENIED',
        message: 'You need to enable access to your contacts.',
        details: null,
      );
    }
  }

  Future<PermissionStatus> _getPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.neverAskAgain) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.storage]);
      return permissionStatus[PermissionGroup.storage] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  Future<String> getCurrentRingtone(Contact contact) async {
    var ringtone;
    PermissionStatus permissionStatus = await _getPermission();
    if (permissionStatus == PermissionStatus.granted) {
      var contactNum = contact.numbers.first;
      try {
        var args = <String, dynamic>{'contactNumber': contactNum};
        ringtone = await _channel.invokeMethod("getRingtone", args);
      } on PlatformException catch (e) {
        print("error : $e");
      }
    } else {
      throw PlatformException(
        code: 'PERMISSION_DENIED',
        message: 'You need to enable access to your contacts.',
        details: null,
      );
    }
    return ringtone;
  }
}
