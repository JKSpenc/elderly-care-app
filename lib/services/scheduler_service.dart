import 'dart:collection';
import 'dart:typed_data';

import 'package:checkup/services/contacts_service.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toast/toast.dart';

class Scheduled {
  List<DayOfWeek> days;
  DateTime start;
  int frequency;
  String unit;
  Contact contact;
  String title;

//  Map<String, dynamic> toMap() {
//    return {'id': id, 'title': title, 'path': path, 'size': size};
//  }

  @override
  String toString() {
    return 'Scheduled{title: $title,'
        ' days: ${days.toString()},'
        ' start: ${start.toString()},'
        ' frequency: $frequency,'
        ' unit: $unit'
        ' contact: ${contact.name},}';
  }
}

class SchedulerService {
  static SchedulerService _instance;

  static const TYPE_CALL = "call";
  static const TYPE_MED_REMINDER = "medication";

  DeviceCalendarPlugin _deviceCalendarPlugin;

  Calendar _calendar;

  Future<Database> _db;

  static SchedulerService getInstance() {
    if (_instance == null) {
      _instance = new SchedulerService();
      _instance._deviceCalendarPlugin = new DeviceCalendarPlugin();
      _instance._db = _setupDatabase();
    }
    return _instance;
  }

  static Future<Database> _setupDatabase() async {
    return openDatabase(
      join(await getDatabasesPath(), 'schedule_database.db'),
      onCreate: (db, version) {
        db.execute(
            '''CREATE TABLE calendar(id TEXT PRIMARY KEY, name TEXT, isReadOnly INTEGER);''');
        db.execute('''CREATE TABLE events(id TEXT PRIMARY KEY, type TEXT);''');
      },
      version: 3,
    );
  }

  Future<void> _insertCalendar(Calendar calendar) async {
    Database db = await _db;

    await db.insert(
      'calendar',
      {
        'id': calendar.id,
        'name': calendar.name,
        'isReadOnly': calendar.isReadOnly
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> _insertEventId(String eventId, String type) async {
    Database db = await _db;

    await db.insert(
      'events',
      {
        'id': eventId,
        'type': type
      },
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> _removeEventId(String eventId) async {
    Database db = await _db;

    await db.delete(
      'events',
      where: 'id = ?',
      whereArgs: [eventId],
    );
  }

  Future<List<String>> _getScheduledEventIds(String type) async {
    Database db = await _db;

    final List<Map<String, dynamic>> eventIds = await db.query(
        'events',
        columns: ['id'],
        where: '"type" = ?',
        whereArgs: [type]
    );

    return eventIds.map((m) => m.values.first.toString()).toList();
  }

  Future<Calendar> _getActiveCalendar() async {
    if (_calendar == null) {
      Database db = await _db;

      final List<Map<String, dynamic>> calendars = await db.query('calendar');
      if (calendars.isEmpty) {
        return null;
      } else {
        // We only expect 1 calendar
        Map<String, dynamic> calendarMap = calendars.first;
        // We need to remap isReadOnly as it is stored as an int
        // (SQLite doesn't support bool)
        calendarMap = Map<String, dynamic>.from(calendarMap).map((k, v) {
          if (k.compareTo('isReadOnly') == 0) {
            bool b = v == 0 ? false : true;
            return MapEntry(k, b);
          } else {
            return MapEntry(k, v);
          }
        });
        _calendar = Calendar.fromJson(calendarMap);
        return Calendar.fromJson(calendarMap);
      }
    } else {
      return _calendar;
    }
  }

  void setActiveCalendar(Calendar calendar) {
    _insertCalendar(calendar);
  }

  Future<Calendar> getActiveCalendar() async {
    return _getActiveCalendar();
  }

  Future<List<Calendar>> getCalendars() async {
    var permissionsGranted = await _deviceCalendarPlugin.hasPermissions();
    if (permissionsGranted.isSuccess && !permissionsGranted.data) {
      permissionsGranted = await _deviceCalendarPlugin.requestPermissions();
      if (!permissionsGranted.isSuccess || !permissionsGranted.data) {
        return [];
      }
    }

    Result<UnmodifiableListView<Calendar>> r =
        await _deviceCalendarPlugin.retrieveCalendars();
    List<Calendar> validCalendarList = List.from(r.data);
    validCalendarList.removeWhere((cal) => cal.isReadOnly);
    return validCalendarList;
  }

  Future<bool> schedule(Scheduled scheduled, String type) async {
    if (scheduled != null &&
        (
            (type == TYPE_CALL && scheduled.contact != null)
            || (type == TYPE_MED_REMINDER && scheduled.title != null)
        ) &&
        scheduled.days.isNotEmpty &&
        scheduled.start != null) {
      Calendar currentCal = await _getActiveCalendar();
      if (currentCal != null) {
        final callEvent = new Event(
          currentCal.id,
          start: scheduled.start,
          title: type == TYPE_CALL ? "Scheduled Call with: ${scheduled.contact.name}" : scheduled.title,
          end: scheduled.start.add(Duration(hours: 1)),
        );
        if (scheduled.unit != null) {
          if (scheduled.unit.compareTo("week(s)") == 0) {
            callEvent.recurrenceRule =
                RecurrenceRule(RecurrenceFrequency.Weekly,
                    interval: scheduled.frequency,
                    daysOfWeek: scheduled.days,
                    endDate: scheduled.start.add(Duration(days: 30)));
          } else {
            callEvent.recurrenceRule =
                RecurrenceRule(RecurrenceFrequency.Monthly,
                    interval: scheduled.frequency,
                    daysOfWeek: scheduled.days,
                    weekOfMonth: WeekNumber.First);
          }
        }

        final createEventResult =
            await _deviceCalendarPlugin.createOrUpdateEvent(callEvent);

        if (createEventResult.isSuccess) {
          _insertEventId(createEventResult.data, type);
        }
        return createEventResult.isSuccess;
      }
    }
    return false;
  }

  Future<List<Event>> getScheduled(String type) async {
    Calendar currentCal = await _getActiveCalendar();
    if (currentCal != null) {
      List<String> eventIds = await _getScheduledEventIds(type);
      Result<UnmodifiableListView<Event>> r =
          await _deviceCalendarPlugin.retrieveEvents(
              currentCal.id,
              RetrieveEventsParams(
                  eventIds: eventIds,
                  startDate: DateTime.now(),
                  endDate: DateTime.now().add(Duration(days: 30))));

      List<Event> mutable = [];
      for (Event event in List<Event>.from(r.data)) {
        if (eventIds.contains(event.eventId) &&
            mutable.where((e) => e.eventId == event.eventId).toList().isEmpty) {
          mutable.add(event);
        }
      }

      return mutable;
    }

    return [];
  }

  Future<bool> removedScheduled(Event event) async {
    Calendar currentCal = await _getActiveCalendar();
    bool removed = false;
    if (currentCal != null && event != null) {
      Result result =
          await _deviceCalendarPlugin.deleteEvent(currentCal.id, event.eventId);
      if (result.data) {
        _removeEventId(event.eventId);
        removed = result.data;
      }
    }
    return removed;
  }
}
