import 'package:checkup/services/contacts_service.dart';
import 'package:checkup/ui/add_contacts.dart';
import 'package:checkup/ui/contact_ringtones.dart';
import 'package:checkup/ui/ok.dart';
import 'package:checkup/ui/reminders.dart';
import 'package:checkup/ui/contacts.dart';
import 'package:checkup/ui/home.dart';
import 'package:checkup/ui/schedule_calls.dart';
import 'package:checkup/ui/set_ringtone.dart';
import 'package:checkup/ui/welcome.dart';

import 'package:flutter/material.dart';


void main() => runApp(new CareApp());

class CareApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Checkup',
      theme: new ThemeData(primarySwatch: Colors.blue),
      home: getLandingPage(),
      routes: <String, WidgetBuilder>{
        // Home
        '/ok': (BuildContext context) => new OKView(),
        '/home': (BuildContext context) => new HomeView(),
        // Welcome UI
        '/welcome': (BuildContext context) => new WelcomeView(),
        // Emergency Contacts UI
        '/contacts': (BuildContext context) => new EmergencyContactsView(),
        '/add_contacts': (BuildContext context) => new AddContactsView(),
        // Set Ringtone UI
        '/contact_ringtones' : (BuildContext context) => new ContactRingtonesView(),
        '/set_ringtone': (BuildContext context) => new SetRingtoneView(),
        // Medication Reminders UI
        '/reminders': (BuildContext context) => new RemindersView(),
        // Schedule Call UI
        '/schedule_calls': (BuildContext context) => new ScheduleCallsView(),
      },
    );
  }
}

Widget getLandingPage() {
  return FutureBuilder<List<Contact>>(
    future: ContactsUtilService.getInstance().databaseContacts(),
    builder: (BuildContext context, AsyncSnapshot<List<Contact>> snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return CircularProgressIndicator();
      } else if (snapshot.data == null || snapshot.data.isEmpty) {
        return WelcomeView();
      } else {
        return OKView();
      }
    }
  );
}

