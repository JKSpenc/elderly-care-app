package spenc.elderlycareapp

import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import android.provider.ContactsContract
import android.provider.MediaStore
import android.widget.Toast
import androidx.annotation.NonNull
import io.flutter.Log
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.io.*
import java.net.URL


class MainActivity : FlutterActivity() {
    private val CHANNEL = "spenc/setringtone"

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            // Note: this method is invoked on the main thread.
            if (call.method == "setRingtone") {
                try {
                    setRingtone(call.argument<String>("path"),
                            call.argument<String>("contactNumber"))
                    result.success(null)
                } catch (e: Exception) {
                    result.error("setRingtone error", e.message, e.stackTrace)
                }

            }

            if (call.method == "getRingtone") {
                try {
                    val ret: String? = getRingtone(call.argument<String>("contactNumber"))
                    result.success(ret)
                } catch (e: Exception) {
                    result.error("getRingtone error", e.message, e.stackTrace)
                }

            }

            if (call.method == "sendText") {
                if (!call.argument<String>("contactNumber").isNullOrBlank()) {
                    val intent = Intent().apply {
                        action = Intent.ACTION_SENDTO
                        data = Uri.parse("smsto:${call.argument<String>("contactNumber")}")
                        putExtra("sms_body", call.argument<String>("messageText"))
                    }
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent)
                    }
                    result.success(null)
                } else {
                    result.error("sendText error", "Number was blank", "")
                }
            }

            if (call.method == "openCallIntent") {
                if (!call.argument<String>("contactNumber").isNullOrBlank()) {
                    val intent = Intent().apply {
                        action = Intent.ACTION_CALL
                        data = Uri.parse("tel:${call.argument<String>("contactNumber")}")
                    }
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent)
                    }
                    result.success(null)
                } else {
                    result.error("openCallIntent error", "Number was blank", "")
                }
            }

        }
    }

    private fun setRingtone(path: String?, contactNumber: String?) {

        var ret = DownloadFileAsync().execute(path, contactNumber).get()

        var file = File(ret)
        if (file.exists()) {
            var oldUri: Uri = MediaStore.Audio.Media.getContentUriForPath(file.absolutePath)
            contentResolver.delete(oldUri, MediaStore.MediaColumns.DATA + "=\"" + file.absolutePath + "\"", null)
            Log.d(CHANNEL, "Deleted old ringtone at $oldUri")

            val projection = arrayOf(
                    ContactsContract.Contacts._ID
            )

            val lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber))

            val ids: Cursor? = contentResolver.query(lookupUri, projection, null, null, null)

            val contactId: Long?

            if (ids != null) {
                val count = ids.count

                if (count > 0) {
                    Log.d(CHANNEL, "Cursor count = ${ids.count}")
                    ids.moveToFirst();
                    Log.d(CHANNEL, "Cursor current position = ${ids.position}")
                    contactId = ids.getLong(ids.getColumnIndex(ContactsContract.RawContacts._ID))
                    Log.d(CHANNEL, "Got contact id $contactId")
                } else {
                    Log.e(CHANNEL, "ID count was 0")
                    contactId = null
                }
            } else {
                Log.e(CHANNEL, "ID cursor was null")
                contactId = null
            }

            if (contactId != null) {

                val values = ContentValues()
                values.put(MediaStore.MediaColumns.DATA, file.absolutePath)
                values.put(MediaStore.MediaColumns.TITLE, "Ringtone for $contactNumber")
                values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3")
                values.put(MediaStore.MediaColumns.SIZE, file.length())
                values.put(MediaStore.Audio.Media.ARTIST, "Care App")
                values.put(MediaStore.Audio.Media.IS_RINGTONE, true)

                val uri: Uri = MediaStore.Audio.Media.getContentUriForPath(file.absolutePath)
                val newUri: Uri? = contentResolver.insert(uri, values)
                Log.d(CHANNEL, "Set new ringtone at $newUri")

                if (newUri != null) {

                    val contactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, contactId.toString())

                    val contentValues = ContentValues()
                    contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, ContactsContract.Contacts.CONTENT_URI.lastPathSegment)
                    contentValues.put(ContactsContract.Contacts.CUSTOM_RINGTONE, newUri.toString())
                    Log.d(CHANNEL, "Setting ringtone path to at $newUri")

                    val updateCount = contentResolver.update(contactUri, contentValues, null, null)
                    Log.d(CHANNEL, "Updated $updateCount contacts with id $contactId and ringtone at $newUri")
                    if (updateCount > 0)
                        Toast.makeText(context, "Successfully updated ringtone!", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun getRingtone(contactNumber: String?): String? {
        var ringtoneName: String? = ""

        val projection = arrayOf(
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.CUSTOM_RINGTONE
        )

        val lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber))

        val ids: Cursor? = contentResolver.query(lookupUri, projection, null, null, null)

        var customRingtone: String? = null

        if (ids != null) {
            val count = ids.count

            if (count > 0) {
                ids.moveToFirst()
                val customRingtoneIndex: Int = ids.getColumnIndex(ContactsContract.Contacts.CUSTOM_RINGTONE)
                customRingtone = ids.getString(customRingtoneIndex)
                Log.d(CHANNEL, "Currently set ringtone $customRingtone")
            } else {
                Log.e(CHANNEL, "ID count was 0")
            }
        } else {
            Log.e(CHANNEL, "ID cursor was null")
        }

        if (customRingtone != null) {
            var uri: Uri = MediaStore.Audio.Media.getContentUriForPath(customRingtone)
            ids!!.close()
            val ringtoneCursor: Cursor? = contentResolver.query(uri, arrayOf(
                    MediaStore.MediaColumns.TITLE,
                    MediaStore.Audio.Media.ARTIST
            ), null, null, null)

            if (ringtoneCursor != null && ringtoneCursor.moveToFirst()) {
                ringtoneName = ringtoneCursor.getString(ringtoneCursor.getColumnIndex(MediaStore.MediaColumns.TITLE))
            }
        }

        return ringtoneName
    }

    internal class DownloadFileAsync : AsyncTask<String?, String?, String?>() {
        override fun doInBackground(vararg aurl: String?): String? {
            var ret: String
            try {
                var cacheDir = File("${Environment.getExternalStorageDirectory()}/spenc.careapp", Uri.encode(aurl[1]))
                if (!cacheDir.exists())
                    cacheDir.mkdirs()

                val url = URL(aurl[0])
                val buf = ByteArray(1024)
                var byteRead: Int
                val f = File("$cacheDir/ringtone.mp3")
                f.createNewFile()
                var outStream = BufferedOutputStream(
                        FileOutputStream(f))

                val conn = url.openConnection()
                var `is` = conn.getInputStream()
                byteRead = `is`!!.read(buf)
                while (byteRead != -1) {
                    outStream.write(buf, 0, byteRead)
                    byteRead = `is`.read(buf)
                }
                `is`.close()
                outStream.close()
                ret = f.absolutePath
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                ret = ""
            }
            return ret
        }
    }
}
