# Checkup

A Flutter app built to assist recall and quality of life for vulnerable people.

## Installation

- Install Flutter by following the steps [here](https://flutter.dev/docs/get-started/install)
- Download and configure Android Studio from the instructions in the previous step
- Run the emulator
